# Author: LuminousLizard
# Licence: EUPL 1.2

#import random
#import xml.etree.ElementTree as ET


class StarsystemGenerator:
    def __init__(self):
        self.starsystem_data = {"Star Class": "Not implemented yet !"}

    def generate_star_system(self):
        """Generates a star system based on attributes from
        "attributes_starsystem.xml" with a random number of
        planets with random attributes based on "attributes_planet.xml".

        Returns:
            text: Attributes of the star system and the yields based on them.
        """
        return self.starsystem_data
