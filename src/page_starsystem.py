# Author: LuminousLizard
# Licence: EUPL 1.2

from nicegui import ui
import generator_starsystem as gs

class StarsystemPage():
    """Star system generation page"""
    def __init__(self) -> None:
        with ui.card():
            ui.markdown("###Star system generator")
            ui.button(text="Generate star system",
                      on_click=self.callback_start_generator)
            self.result = ui.label().style('white-space: pre-wrap')

    def callback_start_generator(self):
        """Creates a 'StarsystemGenerator' instance and calls the
        function to generate a star system with random attributes
        and planets.
        
        Returns:
            string: Attributes and yields
        """
        starsystem = gs.StarsystemGenerator()
        starsystem_data = starsystem.generate_star_system()

        text = ""
        for key, value_list in starsystem_data.items():
            values = ""
            if isinstance(value_list, list):
                for value in value_list:
                    values = values + value + " "
            else:
                values = str(value_list)
            text = text + key + ": " + values + "\n"

        self.result.set_text(text)
