# Author: LuminousLizard
# Licence: EUPL 1.2

import random
import xml.etree.ElementTree as ET


class PlanetGenerator:
    def __init__(self):
        self.planet_data = {"Size": [],
                            "Biome": [],
                            "Climate": [],
                            "Anomaly": [],
                            "Resources": [],
                            "Fertility": [],
                            "Science": [],
                            "Yield: Food": 0,
                            "Yield: Production": 0,
                            "Yield: Credits": 0,
                            "Yield: Research": 0,
                            "Yield: Happiness": 0}

    def generate_planet(self):
        """Generates a planet with random attributes
        based on "attributes_planet.xml".

        Returns:
            text: Attributes of the planet and the yields based on them.
        """
        # Reset yield
        self.planet_data["Yield: Food"] = 0
        self.planet_data["Yield: Production"] = 0
        self.planet_data["Yield: Credits"] = 0
        self.planet_data["Yield: Research"] = 0
        self.planet_data["Yield: Happiness"] = 0

        # Load xml file
        tree = ET.parse("src/attributes_planet.xml")
        root = tree.getroot()

        # Extract attributes from xml file
        attribute_list = list()
        for tag_attribute in root:
            attribute_list.append(tag_attribute.attrib["name"])
            current_attribute_name = tag_attribute.attrib["name"]
            current_attribute_index = attribute_list.index(current_attribute_name)

            # Extract properties of each attribute
            property_list = list()
            for tag_property in tag_attribute:
                property_list.append(tag_property.attrib["name"])

            # Extract base weights and apply modifiers
            data_list = list()
            weights_dict = dict()
            for prop_index, prop_item in enumerate(property_list):
                weights_dict[prop_item] = float(root[current_attribute_index][prop_index][0].text)

                for data_property in root[current_attribute_index][prop_index]:
                    data_list.append(data_property.tag)

                if "weight_modifier" in data_list:
                    for mod in root[current_attribute_index][prop_index].iter("mod"):
                        mod_name = mod.get("name")
                        mod_value = float(mod.get("value"))

                        if mod_name in self.planet_data.values():
                            weights_dict[prop_item] = weights_dict[prop_item] + mod_value
                            if weights_dict[prop_item] < 0:
                                weights_dict[prop_item] = 0

            # Choose attribute by random function
            property_chance = root[current_attribute_index].attrib["probability"].split("/")

            # Dice how many properties can occur
            amount = 0
            chance = random.randrange(0, 101)
            for i in property_chance:
                if chance <= int(i):
                    amount += 1

            # If 1 property can occur and at least one property has a weight > 0
            if (amount == 1) and (sum(v for v in weights_dict.values()) != 0):
                self.planet_data[current_attribute_name] = random.choices(property_list,
                                                                        weights=weights_dict.values(),
                                                                        k=amount)[:]
                print(current_attribute_name + ": " + str(self.planet_data[current_attribute_name]))
            # If more than 1 property can occur and at least one property has a weight > 0
            elif (amount > 1) and (sum(v for v in weights_dict.values()) != 0):
                self.planet_data[current_attribute_name] = random.choices(property_list,
                                                                        weights=weights_dict.values(),
                                                                        k=amount)[:]
                # Tag excluded properties
                # Run through all chosen properties
                for prop in range(0, len(self.planet_data[current_attribute_name])):
                    # If a property is a <excluded> tag from a previous loop, skip this cycle
                    if self.planet_data[current_attribute_name][prop] != "<excluded>":
                        # Search for the current property if it has exclusions
                        iter_var_excluded = root[current_attribute_index][
                            property_list.index(self.planet_data[
                                current_attribute_name][prop])].iter("excluded")
                        # Loop through all found exclusions ...
                        for excluded_item in iter_var_excluded:
                            # and tag all entries of the excluded property with <excluded>
                            while excluded_item.text in self.planet_data[current_attribute_name]:
                                self.planet_data[
                                    current_attribute_name][
                                        self.planet_data[
                                            current_attribute_name].index(
                                                excluded_item.text)] = "<excluded>"
                # Remove doubles (properties and <excluded>)
                self.planet_data[current_attribute_name] = list(
                    set(self.planet_data[current_attribute_name]))
                # Remove last <excluded>
                if "<excluded>" in self.planet_data[current_attribute_name]:
                    self.planet_data[current_attribute_name].remove("<excluded>")
                print(current_attribute_name + ": " + str(self.planet_data[current_attribute_name]))

            # I no property can occur or all weights are zero
            else:
                self.planet_data[current_attribute_name] = ["None specific"]
                print(current_attribute_name + ": " + str(self.planet_data[current_attribute_name]))

            # Calculate yield
            # Contains the current attribute values ?
            if self.planet_data[current_attribute_name]:
                # Run through all properties that are contained in the current attribute
                for i in self.planet_data[current_attribute_name]:
                    # Look for yield values of each property
                    for prop_yield in root[current_attribute_index][property_list.index(i)].iter("yield"):
                        for yield_item in prop_yield:
                            yield_name = "Yield: " + str(yield_item.tag)
                            self.planet_data[yield_name] = self.planet_data[yield_name] + float(yield_item.text)
                            # If calculated yield is smaller than 0, change it to 0
                            if self.planet_data[yield_name] < 0:
                                self.planet_data[yield_name] = 0

        print("====== Planet finished ======")
        return self.planet_data
