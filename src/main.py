# Author: LuminousLizard
# Licence: EUPL 1.2

from nicegui import ui
import page_planet as pp
import page_starsystem as ps

with ui.tabs().classes("w-full") as tabs:
    planet = ui.tab("Planet generator")
    starsystem = ui.tab("Star system generator")

with ui.tab_panels(tabs=tabs, value=planet).classes("w-full"):
    with ui.tab_panel(planet):
        pp.PlanetPage()
    with ui.tab_panel(starsystem):
        ps.StarsystemPage()

ui.run()
