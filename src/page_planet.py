# Author: LuminousLizard
# Licence: EUPL 1.2

from nicegui import ui
import generator_planet as gp

class PlanetPage():
    """Planet generation page"""
    def __init__(self) -> None:
        with ui.card():
            ui.markdown("###Planet generator")
            ui.button(text="Generate planet",
                      on_click=self.callback_start_generator)
            self.result = ui.label().style('white-space: pre-wrap')

    def callback_start_generator(self):
        """Creates a 'PlanetGenerator' instance and calls the
        function to generate a planet with random attributes.
        
        Returns:
            string: Attributes and yields
        """
        planet = gp.PlanetGenerator()
        planet_data = planet.generate_planet()

        text = ""
        for key, value_list in planet_data.items():
            values = ""
            if isinstance(value_list, list):
                for value in value_list:
                    values = values + value + " "
            else:
                values = str(value_list)
            text = text + key + ": " + values + "\n"

        self.result.set_text(text)
