# PlanetGenerator

A program that creates planets from a list of properties, like in 4X space games.

![Example](example.png)

## Generation process and attributes
### Generation flow

```mermaid
flowchart LR;
Size-->Biome-->Climate-->Anomaly-->Resources-->Fertility-->Science;
```
<br>
A choice can depend on the previously selected attributes. For example an anomaly can depend on the climate, biome and size of the planet.

### Planet attributes
Following attributes are currently contained in the xml file and used by the generator:

|Size   |Biome      |Climate            |Anomaly              |Resources                |Fertility      |Science
|----   |---        |---                |---                  |---                      |---            |---        |
|Tiny   |Boreal     |Heavy precipitation|No magnetic field    |Gemstones                |Black soil     |Ancient ruins
|Small  |Desert     |Blizzards          |Kessler syndrome    |Metal-rich               |Low nutrient   |High biodiversity (flora)
|Medium |Subtropic  |Sand storms        |Global aurora        |High geothermal potential|Dour           |High biodiversity (fauna)
|Large  |Tropic     |Long winter        |Strong vulcanism     |High hydropower potential|Special habitats|Spaceship Graveyard
|Huge   |Snow       |Long summer        |No vulcanism         |High wind power potential|               |
|       |Arctic     |Only winter        |Strong plate movement|Hydrogen atmosphere
|       |Steppe     |Only summer        |High gravitation     |
|       |Ocean      |Ash rain           |Low gravitation      |
|       |Highlands  |Tropical cyclone   |Glowing plants       |
|       |Gas planet |Hypercane          |Psychiadelic atmospheres|                      |               |
|       |Vulcanic   |Heat waves         |
|       |Plains     |Megadrought
|       |Radioactive Wasteland|Acid rain|
|-      |-          |None specific      |None specific        |None specific            |None specific  |None specific

Many attributes have probabilities also in relation to previously selected attributes. 
E.g.: Blizzards have a higher probability at "snow" and "arctic" planets and cannot occur on desert planets.

Some attributes are mutually exclusive.
E.g. a planet can have 2 anomalies but of course not "low gravitation" and "high gravitation".

For more details see "planet_attributes.xml".

### Yield
Also there is a yield calculated depending on the choosen attributes:
- Food
- Production
- Credits
- Research
- Happiness

The values for each attribute can also be found in the xml and are currently still kept very simple.

## Star system generation
WIP ! Not implemented yet.
### Star attributes
|Classes|
|----|
|O-Class
|B-Class
|A-Class
|F-Class
|G-Class
|K-Class
|M-Class

## Explanation "attributes.xml"
```xml
<data>
    <attribute name="Anomaly" probability="25/10">   <!-- Attribute with probability of occurrence.-->
                                                     <!-- In case of multiple values, multiple attributes can occur.-->
                                                     <!-- here: First anomaly with a 25% and a second with a 10 % chance.-->

            <property name="No vulcanism">           <!-- Property name-->

                <base_weight>1</base_weight>         <!-- Base weight for choice, if all properties have 1-->
                                                     <!-- then all properties have the same chance.-->
                                                     <!-- Can be a positive int or float any size.-->

                <weight_modifier>                    <!-- Weight modifier depending on previously selected attributes.-->
                    <mod name="Vulcanic" value="-1"/><!-- If biome "Vulcanic" is chosen, the the base weight-->
                                                     <!-- is decreased by 1 (to 0 = no chance for choice).-->
                    <mod name="Gas planet" value="10"/>
                </weight_modifier>

                <excluded>Strong vulcanism</excluded><!-- Properties that are mutually exclusive.-->
                                                     <!-- May be included multiple times.-->
                </excluded>
                <yield>                              <!-- Yield bonus for the planet for this property.-->
                    <Food>1</Food>
                    <Production>1</Production>
                    <Credits>1</Credits>
                    <Research>0</Research>
                    <Happiness>0</Happiness>
                </yield>
            </property>
    </attribute>
</data>
```

## Requirements
* Python 3.11 (maybe other Python 3 versions also work)
* NiceGUI

## Licence
EUPL 1.2
